import logo from './lg_spinner.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Sun Spinner - version 0.0.1
        </p>
        <a
          className="App-link"
          href="https://sunteco.vn/sun-spinner/"
          target="_blank"
          rel="noopener noreferrer"
        >
          Document here
        </a>
      </header>
    </div>
  );
}

export default App;
